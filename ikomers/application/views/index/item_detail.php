<center><h4 ><i class="fa fa-search-plus"></i>Detail Item</h4></center>
<hr>
<br>
<?php
$key = $data->row();
?>
<div class="row">
   <div class="col m3 s12 offset-m1">
      <img src="<?php echo base_url(); ?>/asset/upload/<?php echo $key->gambar_item ?>" alt="" class="responsive-img">
   </div>
   <div class="col m7 s12 offset-m1">
      <!-- detail item -->
      <table class="responsive-table bordered striped">
         <tr>
            <td style="width: 30%; text-align: left; vertical-align: top">Nama Barang: </td>
            <td><?php echo ucfirst($key->nama_item); ?></td>
         </tr>
         <tr>
            <td style="width: 30%; text-align: left; vertical-align: top">Harga Barang: </td>
            <td>Rp <?php echo number_format($key->harga_item, 0, ',','.'); ?></td>
         </tr>
         <tr>
            <td style="width: 30%; text-align: left; vertical-align: top">Berat Barang: </td>
            <td><?php echo ucfirst($key->berat_item); ?></td>
         </tr>
         <tr>
            <td style="width: 30%; text-align: left; vertical-align: top">Jumlah Stok: </td>
            <td><?php echo ucfirst($key->jumlah_item); ?></td>
         </tr>
         <tr>
            <td style="width: 30%; text-align: left; vertical-align: top">Deskripsi barang: </td>
            <td><?php echo ucfirst(nl2br($key->desk_item)); ?></td>
         </tr>
      </table>
      <br>
      <button type="button" class="btn red waves-effect waves-light" onclick="window.history.go(-1)">Kembali</button>
      <a href="<?php echo base_url(); ?>cart/add/<?php echo $key->id_item ?>" class="btn blue waves-effect waves-light"><i class="fa fa-shopping-cart"></i> Tambahkan ke Keranjang</a>
   </div>
</div>

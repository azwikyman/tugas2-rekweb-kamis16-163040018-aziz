<div class="row">
    <div class="col m10 s12 offset-m1">
        <h4 style="color:939393;"><i class="fa fa-pencil-square-o"></i> Form Registrasi</h4>
        <hr>
        <br>
        <?php echo validation_errors('<p style="color:red">', '</p>'); ?>
        <form action="" method="post">
            <div class="col m10 s12">
                <div class="row">
                    <div class="input-field col m6 s12">
                        <input type="text" id="nama_depan" class="validate" name="nama1" value="<?php echo $nama1; ?>">
                        <label for="nama_depan">Nama Depan</label>
                    </div>
                    <div class="input-field col m6 s12">
                        <input type="text" id="nama_belakang" class="validate" name="nama2" value="<?php echo $nama2; ?>">
                        <label for="nama_belakang">Nama Belakang</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m8 s12">
                        <input type="text" id="username" class="validate" name="user" value="<?php echo $user; ?>">
                        <label for="username">Username</label>
                    </div>
                    <div class="input-field col m8 s12">
                        <input type="email" id="email" class="validate" name="email" value="<?php echo $email; ?>">
                        <label for="email">E-Mail</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m6 s12">
                        <input type="password" id="password" class="validate" name="pass1">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col m6 s12">
                        <input type="password" id="password" class="validate" name="pass2">
                        <label for="password">Ketik Ulang Password</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <label>Jenis Kelamin</label>
                        <br>
                        <p>
                            <input class="with-gap" value="L" type="radio" id="laki" name="jk" <?php if($jk == 'L') { echo 'checked';} ?>>
                            <label for="laki">Laki-Laki</label>
                             <input class="with-gap" value="P" type="radio" id="perempuan" name="jk" <?php if($jk == 'P') { echo 'checked';} ?>>
                            <label for="perempuan">Perempuan</label>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m8 s12">
                        <input type="number" id="telp" class="validate" name="telepon" value="<?php echo $telepon; ?>">
                        <label for="telepon">Nomor Telepon</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea name="alamat" id="alamat" class="materialize-textarea"><?php echo $alamat; ?></textarea>
                        <label for="alamat">Alamat</label>
                    </div>
                </div>

                <div class="row right">
                    <button type="button" onclick="window.history.go(-1)" class="btn red waves-effect waves-light">Kembali</button>
                    <button type="submit" class="btn blue waves-effect waves-light" name="submit" value="submit">Submit <i class="fa fa-paper-plane"></i></button>
                </div>

            </div>
        </form>
    </div>
</div>

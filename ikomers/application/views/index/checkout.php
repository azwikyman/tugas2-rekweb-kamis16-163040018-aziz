<div class="row">
    <div class="col m10 s12 offset-m1">
        <h4 style="color: #939393"><i class="fa fa-shopping-bag"></i> Checkout</h4>
        <hr>
        <br>
        <?php echo validation_errors('<p style="color:red">', '</p>'); ?>
        <form action="" method="post">
            <div class="col m10 s12">

                <div class="row">
                    <div class="col m8 s12">
                        <label>Provinsi</label>
                        <select name="prov" class="browser-default">
                            <option value="" disabled selected>--Pilih Provinsi--</option>
                        </select>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col m8 s12">
                        <label>Pilih kota/kabupaten</label>
                        <select name="kota" class="browser-default">
                            <option value="" disabled selected>--Kota/Kabupaten--</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m8 s12">
                        <input type="text" id="alamat" class="validate" name="alamat" value="">
                        <label for="alamat">Alamat</label>
                    </div>
                    <div class="input-field col m4 s12">
                        <input type="number" id="kd_pos" class="validate" name="kd_pos" value="">
                        <label for="kd_pos">Kode Pos</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col m8 s12">
                        <label>Pilih kurir</label>
                        <select name="kurir" class="browser-default">
                            <option value="" disabled selected>--Kota/Kabupaten--</option>
                            <option value="pos">POS</option>
                            <option value="jne">JNE</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col m8 s12">
                        <label>Pilih layanan</label>
                        <select name="layanan" class="browser-default">
                            <option value="" disabled selected>--Pilih Layanan--</option>
                        </select>
                    </div>
                    <div class="col m4 s12">
                        <label>Ongkos Kirim</label>
                        <input type="number" name="ongkir" value="0">
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m4 s12 offset-m8">
                        <input type="number" name="total" value="<?php echo $this->cart->total(); ?>">
                        <label>Total Biaya</label>
                    </div>
                </div>
                <br>

                <div class="row right">
                    <button type="button" onclick="window.history.go(-1)" class="btn red waves-effect waves-light">Kembali</button>
                    <button type="submit" name="submit" value="submit" class="btn blue waves-effect waves-light">Submit <i class="fa fa-paper-plane"></i></button>
                </div>

            </div>
        </form>
    </div>
</div>

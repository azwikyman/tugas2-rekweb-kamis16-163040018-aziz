<div class="row">
  <?php foreach ($data->result() as $key) : ?>
  <div class="col s12 m3">
    <div class="card">
      <div class="card-image">
        <img src="<?php echo base_url(); ?>asset/upload/<?php echo $key->gambar_item;?>">
        <span class="card-title">Rp <?php echo number_format($key->harga_item, 0, ',','.'); ?></span>
      </div>
      <div class="card-content">
        <p><?php echo $key->nama_item; ?></p>
      </div>
      <div class="card-action">
        <a href="<?php echo site_url(); ?>home/detail/<?php echo $key->id_item ?>" class="waves-effect waves-light btn-flat blue white-text"><i class="fa fa-search-plus"></i> Detail</a>
        <a href="<?php echo base_url(); ?>cart/add/<?php echo $key->id_item ?>" class="waves-effect waves-light btn-flat green white-text"><i class="fa fa-shopping-cart"></i> Add to cart</a>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>

<div class="x_panel">
    <div class="x_title">
        <h2>Detail Item</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <img src="<?php echo base_url(); ?>/asset/upload/<?= $gambar_item;?>" alt="" style="width: 100%">
            </div>
            <div class="col-md-6 col-sm-6">
                <table class="table table-striped">
                    <tr>
                        <td width="100px">Nama Item</td>
                        <td>: <?php echo $nama_item; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Harga Item</td>
                        <td>: <?php echo 'Rp' . number_format($harga_item, 0, ',','.'); ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Jumlah Item</td>
                        <td>: <?php echo $jumlah_item; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Berat Item</td>
                        <td>: <?php echo $berat_item; ?> kg</td>
                    </tr>
                    <tr>
                        <td width="100px">Status Item</td>
                        <td>: <?php if($status_item == 1) {
                             echo "Aktif";
                        } else {
                            echo "Tidak aktif";
                        }
                        ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Deskripsi</td>
                        <td>: <?php echo nl2br($desk_item); ?></td>
                    </tr>
                </table>
                <a href="#" class="btn btn-primary" onclick="window.history.go(-1)">Kembali</a>
            </div>
        </div>
    </div>
</div>

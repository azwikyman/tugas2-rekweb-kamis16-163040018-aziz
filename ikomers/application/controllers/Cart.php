<?php 
class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_user');
    }

    public function index() {
        $this->template->fend('index/index', 'index/cart');
    }

    public function add() {
        if(is_numeric($this->uri->segment(3))) {
            $id = $this->uri->segment(3);
            $get = $this->app_user->get_where('barang', array('id_item' => $id))->row();

            $data = array(
                'id' =>$get->id_item,
                'name' => $get->nama_item,
                'price' => $get->harga_item,
                'weight' => $get->berat_item,
                'qty' => 1
            );
            $this->cart->insert($data);

            echo '<script type="text/javascript">window.history.go(-1);</script>';
            
        } else {
            redirect('home');
        }
    }

    public function update() {
        if($this->uri->segment(3)) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('qty', 'Jumlah Pesananan', 'required|numeric');
            if($this->form_validation->run() == TRUE) {
                $data = array(
                    'rowid' => $this->uri->segment(3),
                    'qty'   => $this->input->post('qty', TRUE)
                );
                
                $this->cart->update($data);
                redirect('cart');
            } else {
                $this->template->fend('index/index', 'index/cart');
            }
        } else {
            redirect('cart');
        }
    }

    public function delete() {
        if($this->uri->segment(3)) {
            $rowid = $this->uri->segment(3);
            $this->cart->remove($rowid);

            redirect('cart');    
        } else {
            redirect('cart');
        }
    }
}

?>

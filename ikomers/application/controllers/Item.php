<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_admin');
    }


    public function index() {
        $data['data'] = $this->app_admin->get_all('barang');
        $this->template->admin('admin/dashboard', 'admin/manage_item', $data);
    }

    public function add_item() {
        if($this->input->post('submit', TRUE) == 'submit') {
            //validasi input
            $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
            $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
            $this->form_validation->set_rules('berat', 'Berat Item', 'required|numeric');
            $this->form_validation->set_rules('status', 'Status Item', 'required|numeric');
            $this->form_validation->set_rules('desk', 'Deskripsi Item', 'required|min_length[4]');
            $this->form_validation->set_rules('jumlah', 'Jumlah Stok', 'required|numeric');


            if ($this->form_validation->run() == TRUE) {
                $config['upload_path'] = './asset/upload';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '2048';
                $config['file_name'] = 'gambar' . time();

                $this->load->library('upload', $config);

                if($this->upload->do_upload('foto')) {
                    $gbr = $this->upload->data();
                    //proses insert
                    $items = array(
                    'nama_item' => $this->input->post('nama', TRUE),
                    'harga_item' => $this->input->post('harga', TRUE),
                    'berat_item' => $this->input->post('berat', TRUE),
                    'jumlah_item' => $this->input->post('jumlah', TRUE),
                    'status_item' => $this->input->post('status', TRUE),
                    'gambar_item' => $gbr['file_name'],
                    'desk_item' => $this->input->post('desk', TRUE)
                );
                    $this->app_admin->insert('barang', $items);
                     $this->session->set_flashdata('insert', 'Data berhasil ditambahkan!');
                    redirect('item');
                } else {
                    $this->session->set_flashdata('alert', 'Masukkan foto produk!');
                }
            }
        }
        $data['header'] = "Tambah Barang";
        $data['nama'] = $this->input->post('nama', TRUE);
        $data['berat'] = $this->input->post('berat', TRUE);
        $data['harga'] = $this->input->post('harga', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $data['desk'] = $this->input->post('desk', TRUE);
        $data['jumlah'] = $this->input->post('jumlah', TRUE);

      
        $this->template->admin('admin/dashboard', 'admin/item_form', $data);
    }

    public function delete($id) {
        $this->app_admin->delete($id);
        $this->session->set_flashdata('alert', 'Data berhasil dihapus!');
        redirect('item');
    }

    public function detail() {
        $id_item = $this->uri->segment(3);
        $item = $this->app_admin->get_where('barang', array('id_item' => $id_item));

        foreach ($item -> result() as $key) {
            $data['nama_item'] = $key->nama_item;
            $data['harga_item'] = $key->harga_item;
            $data['jumlah_item'] = $key->jumlah_item;
            $data['berat_item'] = $key->berat_item;
            $data['status_item'] = $key->status_item;
            $data['gambar_item'] = $key->gambar_item;
            $data['desk_item'] = $key->desk_item;

        }
        $this->template->admin('admin/dashboard', 'admin/detail_item', $data);
    }

    public function update_item() {
        $id_item = $this->uri->segment(3);
        if($this->input->post('submit', TRUE) == 'submit') {
            //validasi input
            $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
            $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
            $this->form_validation->set_rules('berat', 'Berat Item', 'required|numeric');
            $this->form_validation->set_rules('status', 'Status Item', 'required|numeric');
            $this->form_validation->set_rules('desk', 'Deskripsi Item', 'required|min_length[4]');
            $this->form_validation->set_rules('jumlah', 'Jumlah Stok', 'required|numeric');


            if ($this->form_validation->run() == TRUE) {
                $config['upload_path'] = './asset/upload';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '2048';
                $config['file_name'] = 'gambar' . time();

                $this->load->library('upload', $config);
                $items = array(
                        'nama_item' => $this->input->post('nama', TRUE),
                        'harga_item' => $this->input->post('harga', TRUE),
                        'berat_item' => $this->input->post('berat', TRUE),
                        'jumlah_item' => $this->input->post('jumlah', TRUE),
                        'status_item' => $this->input->post('status', TRUE),
                        'desk_item' => $this->input->post('desk', TRUE)
                    );

                if($this->upload->do_upload('foto')) {
                    $gbr = $this->upload->data();
                    //proses insert
                    unlink('asset/upload/'.$this->input->post('old_pict', TRUE));
                    $items['gambar_item'] = $gbr['file_name'];
                    $this->app_admin->update('barang', $items, array('id_item' => $id_item));
                } else {
                    $this->app_admin->update('barang', $items, array('id_item' => $id_item));
                }
            }
            $this->session->set_flashdata('upload', 'Data berhasil diupdate!');
            redirect('item');
        }

        $item = $this->app_admin->get_where('barang', array('id_item' => $id_item));

        foreach ($item->result() as $key) {
            $data['nama'] = $key->nama_item;
            $data['berat'] = $key->berat_item;
            $data['harga'] = $key->harga_item;
            $data['status'] = $key->status_item;
            $data['desk'] = $key->desk_item;
            $data['jumlah'] = $key->jumlah_item;
            $data['gambar'] = $key->gambar_item;
        }
        $data['header'] = "Ubah Barang";
        

      
        $this->template->admin('admin/dashboard', 'admin/item_form', $data);
    }
}
